<?php

namespace App\Utils;

class JsonFormat
{

    public static function response($data,$statusCode=200,$message=null,$error=null)
    {
        $format = [
            'data' => $data,
            'message' => $message,
            'error' => $error,
            'total' => 0,
            'statusCode' => $statusCode,
        ];

        return response()->json($format,200);

    }


}

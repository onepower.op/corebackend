<?php

namespace App\Utils;

class StatusCode
{
        const SUCCESS = ['value'=>200,'desc'=>"Success"];
        const BAD_REQUEST = ['value'=>400,'desc'=>"Bad request"];
        const NOT_FOUND =  ['value'=>404,'desc'=>"Not found"];
        const UNAUTHORIZED = ['value'=>401,'desc'=>"Unauthorized"];
        const FORBIDDEN = ['value'=>403,'desc'=>"Forbidden"];
        const INTERNAL_ERROR =  ['value'=>500 ,'desc'=>"Internal Server Error"];
}

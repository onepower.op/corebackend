<?php

namespace Channy\Corebackend;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;


class CorebackendServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
    }
    public function register()
    {
    }

}

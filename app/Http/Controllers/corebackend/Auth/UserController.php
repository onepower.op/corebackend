<?php

namespace App\Http\Controllers\corebackend\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Utils\JsonFormat;
use App\Utils\StatusCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function register(Request $request){
        $validator = Validator::make(
            $request->all(),
            [
                'username' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
            ],
        );

        if($validator->fails()){
            return JsonFormat::response(null,StatusCode::BAD_REQUEST['value'],$validator->errors());
        }

        $request['password'] = bcrypt($request->password);

        $user = User::create($request->all());

        $user['token'] = $user->createToken('AuthLaravel')->accessToken;

        return JsonFormat::response($user,StatusCode::SUCCESS['value']);
    }

    public function update(Request $request,$id)
    {

        $data = $request->all();
        $user = User::where('id',$id)->first();

        if($user){
            $user->update($data);
            return JsonFormat::response($user);
        }
        return JsonFormat::response(null,StatusCode::NOT_FOUND['value']);
    }








}

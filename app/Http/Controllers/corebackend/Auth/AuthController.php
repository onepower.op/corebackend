<?php

namespace App\Http\Controllers\corebackend\Auth;

use App\Http\Controllers\Controller;
use App\Utils\JsonFormat;
use App\Utils\StatusCode;
use Illuminate\Http\Request;

class AuthController extends Controller
{


    public function currentUser(Request $request)
    {
        $user = auth()->guard('api')->user();
        return JsonFormat::response($user,StatusCode::SUCCESS['value']);
    }

    public function login(Request $request)
    {
        $data = [
            'username' => $request->username,
            'password' => $request->password
        ];

        if(auth()->attempt($data)){
            $user = auth()->user();
            $user['token'] = $user->createToken('AuthLaravel')->accessToken;
            return JsonFormat::response($user,StatusCode::SUCCESS['value']);
        }else{
            return JsonFormat::response(null,StatusCode::UNAUTHORIZED['value'],"Unauthorized");
        }

    }
    public function logout(Request $request)
    {
        if(auth()->check()){
            auth()->user()->token()->revoke();
            return JsonFormat::response(null,StatusCode::SUCCESS['value']);
        }else{
            return JsonFormat::response(null,StatusCode::SUCCESS['value'],"Failed! You're already logout.");
        }
    }
}
